# open file
f = open("input.txt", "r")

# add sum to array
highest = 0
current = 0
cal = []
for l in f:
    if l == '\n':
        cal.append(current)
        current = 0
        continue
    current += int(l)

# sum of biggest 3
cal.sort(reverse=True)
sum = cal[0] + cal[1] + cal[2]

print(sum)