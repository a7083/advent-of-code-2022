# Advent Of Code 2022
Advent Of Code is a daily challenge for programmers to solve programming problems. The difficulty can range from easy to very complex.
For more information visit https://adventofcode.com/

## My participation
This is the second time I try to participate. Due to my job and university I only have limited time to spend on this. 

## Techstack
Since I became more familiar with Python lately, I am going to use it to solve the easy challenges. For performance critical problems I tend to use C++.