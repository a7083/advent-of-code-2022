
# open file
f = open("input.txt", "r")

# for each line
finishedLoading = False
#for l in f:
l = f.read()
    # for each char
    
start = 0
end = 4
for i in range(0, len(l)-3):
    # check previous chars
    sub = l[start:end]
    # print("Checking", sub)
    exists = False
    for ci in range(4):
        for cii in range(4):
            if ci == cii:
                continue
            if sub[ci] == sub[cii]:
                exists = True
    if not exists:
        print("Found correct sub", sub, end)
        break
    start += 1
    end += 1