
# open file
f = open("input.txt", "r")

# 2d array
w = 99
h = 99
trees = [[0 for x in range(w)] for y in range(h)]
visible = [[0 for x in range(w)] for y in range(h)]
for x in range(w):
    for y in range(h):
        visible[y][x] = False
        if x == 0 or x == w-1 or y == 0 or y == h-1:
            visible[y][x] = True

# input
y = 0
for l in f:
    x = 0
    for c in l:
        if c == '\n':
            break
        trees[y][x] = int(c)
        x += 1
    y += 1   

# check if hidden
for y in range(1, h-1):
    for x in range(1, w-1):
        if visible[y][x]:
            continue
        # check horizontal
        hasBiggerOne = False
        for xx in range(x+1, w):
            if trees[y][x] <= trees[y][xx]:
                # print(y, x, xx, trees[y][x], trees[y][xx])
                hasBiggerOne = True
                break
        if not hasBiggerOne:
           visible[y][x] = True
           continue
        
        hasBiggerOne = False
        for xx in range(0, x):
            if trees[y][x] <= trees[y][xx]:
                # print(y, x, xx, trees[y][x], trees[y][xx])
                hasBiggerOne = True
                break
        if not hasBiggerOne:
           visible[y][x] = True
           continue
        
        # check vertical
        hasBiggerOne = False
        for yy in range(y+1, h):
            if trees[y][x] <= trees[yy][x]:
                # print(y, x, yy, trees[y][x], trees[yy][x])
                hasBiggerOne = True
                break
        if not hasBiggerOne:
           visible[y][x] = True
           continue
        
        hasBiggerOne = False
        for yy in range(0, y):
            if trees[y][x] <= trees[yy][x]:
                # print(y, x, yy, trees[y][x], trees[yy][x])
                hasBiggerOne = True
                break
        if not hasBiggerOne:
           visible[y][x] = True
           continue

# count visible trees
count = 0
for x in range(w):
    for y in range(h):
        if visible[y][x]:
            count += 1
            
# print(trees)
# print(visible)
print("Amount:", count)