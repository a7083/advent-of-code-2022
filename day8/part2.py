# open file
f = open("input.txt", "r")

# 2d array
w = 99
h = 99
trees = [[0 for x in range(w)] for y in range(h)]

# input
y = 0
for l in f:
    x = 0
    for c in l:
        if c == '\n':
            break
        trees[y][x] = int(c)
        x += 1
    y += 1   

# check highest
highest = 0
for y in range(0, h):
    for x in range(0, w):
        
        # check horizontal
        xx = x+1
        sees1 = 0
        while xx < w:
            sees1 += 1
            if trees[y][x] <= trees[y][xx]:
                break
            xx += 1
        #print("Tree", y, x, "sees", sees1, "trees to the right")
        
        xx = x-1
        sees2 = 0
        while xx >= 0:
            sees2 += 1
            if trees[y][x] <= trees[y][xx]:
                break
            xx -= 1
        #print("Tree", y, x, "sees", sees2, "trees to the left at pos xx", xx)
        
        # check vertical
        yy = y+1
        sees3 = 0
        while yy < h:
            sees3 += 1
            if trees[y][x] <= trees[yy][x]:
                break
            yy += 1
        #print("Tree", y, x, "sees", sees3, "trees to the bottom at pos xx", xx)
        
        yy = y-1
        sees4 = 0
        while yy >= 0:
            sees4 += 1
            if trees[y][x] <= trees[yy][x]:
                break
            yy -= 1
        #print("Tree", y, x, "sees", sees4, "trees to the top at pos xx", xx)

        total = sees1 * sees2 * sees3 * sees4
        if total > highest:
            highest = total
       
            
print("Highest", highest)