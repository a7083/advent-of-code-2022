
# open file
f = open("input.txt", "r")

# var
crate_amount = 9
crates = []
for i in range(crate_amount):
    crates.append([])

finishedLoading = False
for l in f:

    if not finishedLoading:
        # load crates
        char_index = 0
        crate_index = 0
        for c in l:
            # if its number 
            if c == '1':
                finishedLoading = True

                # reverse list, make it a stack
                for i in range(crate_amount):
                    crates[i] = list(reversed(crates[i]))
                break

            # otherwise add to list
            if (char_index+3) % 4 == 0 \
                and crate_index < crate_amount:
                if c != ' ':
                    crates[crate_index].append(c)
                crate_index += 1
            char_index += 1
    else:
        # ignore margin line
        if (l[0] == '\n'):
            continue
        line = l[5:len(l)-1]

        # extrude numbers
        c_move = int(line[:line.find(' ')])
        line = line[line.find(' ')+6:]
        c_from = int(line[:line.find(' ')])-1
        line = line[line.find(' ')+1:]
        c_to = int(line[3:])-1

        # apply to stack
        tmp = []
        for i in range(c_move):
            c = crates[c_from].pop()
            tmp.append(c)
        tmp = list(reversed(tmp))
        crates[c_to].extend(tmp)
# End
for i in range(crate_amount):
    print(crates[i][len(crates[i])-1], end="")
print("")
f.close()