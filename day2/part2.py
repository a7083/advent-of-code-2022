# open file
f = open("input.txt", "r")

# X for Rock (+1 P)
# Y for Paper (+2 P)
# Z for Scissors (+3 P)

# X means you need to lose
# Y means you need to end the round in a draw
# Z means you need to win

# A for Rock
# B for Paper
# C for Scissors

# Right side win: +6 P
# Draw: +3 P

# each line
totalScore = 0
for line in f:
    total = 0
    l = list(line)
    print("t", l)

    # Adjust choice to part 2
    if l[2] == "Y": #draw
        if l[0] == "A":
            l[2] = 'X'
        if l[0] == "B":
            l[2] = "Y"
            print("e")
        if l[0] == "C":
            l[2] = "Z"
    elif l[2] == "X": #loose
        if l[0] == "A":
            l[2] = 'Z'
        if l[0] == "B":
            l[2] = "X"
        if l[0] == "C":
            l[2] = "Y"
            print("s")
    elif l[2] == 'Z': #win
        if l[0] == "A":
            l[2] = "Y"
            print("a")
        if l[0] == "B":
            l[2] = "Z"
        if l[0] == "C":
            l[2] = "X"
    print("t", l)

    # Points add
    if l[2] == 'X':
        total += 1
    if l[2] == 'Y':
        total += 2
    if l[2] == 'Z':
        total += 3
        
    # Draw add
    if  (l[2] == "X" and l[0] == "A") or \
        (l[2] == "Y" and l[0] == "B") or \
        (l[2] == "Z" and l[0] == "C"):
        total += 3

    # Win add
    if  (l[2] == "X" and l[0] == "C") or \
        (l[2] == "Y" and l[0] == "A") or \
        (l[2] == "Z" and l[0] == "B"):
        total += 6
    print("line Score:", total)
    totalScore += total

# End
f.close()
print("TotalScore", totalScore)