# open file
f = open("input.txt", "r")

# X for Rock (+1 P)
# Y for Paper (+2 P)
# Z for Scissors (+3 P)

# A for Rock
# B for Paper
# C for Scissors

# Right side win: +6 P
# Draw: +3 P

# each line
total = 0
for l in f:
    # Points add
    if l[2] == 'X':
        total += 1
    if l[2] == 'Y':
        total += 2
    if l[2] == 'Z':
        total += 3
        
    # Draw add
    if  (l[2] == "X" and l[0] == "A") or \
        (l[2] == "Y" and l[0] == "B") or \
        (l[2] == "Z" and l[0] == "C"):
        total += 3
        continue

    # Win add
    if  (l[2] == "X" and l[0] == "C") or \
        (l[2] == "Y" and l[0] == "A") or \
        (l[2] == "Z" and l[0] == "B"):
        total += 6

# End
print(total)