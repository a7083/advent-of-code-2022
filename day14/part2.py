# config symbols
AIR = '.'
ROCK = '#'
SAND = 'o'

# read file
f = open("input.txt", "r")
lines = f.readlines()

# var
w = 700
h = 400
grid = [[AIR for i in range(w)] for j in range(h)] 

# display grid
def display():
    for y in range(h):
        print(y, " ", end="")
        for x in range(494, 504):
            print(grid[y][x], end="")
        print("")

# for each line
biggestY = 0
for line in lines:
    l = line.replace("\n", "")
    instr = l.split(" -> ")
    #print(instr)
    for i in range(len(instr)-1):
        # get values
        fromx = (int) (instr[i].split(",")[0])
        fromy = (int) (instr[i].split(",")[1])
        tox = (int) (instr[i+1].split(",")[0])
        toy = (int) (instr[i+1].split(",")[1])

        if fromx > tox or fromy > toy:
            tmpx = fromx
            fromx = tox
            tox = tmpx
            tmpy = fromy
            fromy = toy
            toy = tmpy

        # biggestY
        if toy > biggestY:
            biggestY = toy

        # add rock
        if fromx == tox:
            for y in range(fromy, toy+1):
                grid[y][fromx] = ROCK
        if fromy == toy:
            for x in range(fromx, tox+1):
                grid[fromy][x] = ROCK

# create bottom
for x in range(0, w):
    grid[biggestY+2][x] = ROCK
#display()

# sand produce physics
s = 0
while True:
    # Sand position
    posX = 500
    posY = 0

    # check fall
    while True:
        # apply physics
        if grid[posY+1][posX] == AIR:
            posY += 1
        elif grid[posY+1][posX-1] == AIR:
            posY += 1
            posX -= 1
        elif grid[posY+1][posX+1] == AIR:
            posY += 1
            posX += 1
        else:
            # no place to fall
            grid[posY][posX] = SAND
            break
    
    # if end
    if posX == 500 and posY == 0:
        print("Spawned sand nr", s+1, "is the first one falling in void")
        break
        
    # Debug
    #print("Spawned Sand", s+1)
    #display()
    s += 1