# config symbols
AIR = '.'
ROCK = '#'
SAND = 'o'

# read file
f = open("input.txt", "r")
lines = f.readlines()

# var
w = 600
h = 300
grid = [[AIR for i in range(w)] for j in range(h)] 

# display grid
def display():
    for y in range(h):
        print(y, " ", end="")
        for x in range(494, 504):
            print(grid[y][x], end="")
        print("")

# for each line
for line in lines:
    l = line.replace("\n", "")
    instr = l.split(" -> ")
    print(instr)
    for i in range(len(instr)-1):
        # get values
        fromx = (int) (instr[i].split(",")[0])
        fromy = (int) (instr[i].split(",")[1])
        tox = (int) (instr[i+1].split(",")[0])
        toy = (int) (instr[i+1].split(",")[1])

        print("Before", fromx, fromy, tox, toy)
        if fromx > tox or fromy > toy:
            tmpx = fromx
            fromx = tox
            tox = tmpx
            tmpy = fromy
            fromy = toy
            toy = tmpy

        # add rock
        if fromx == tox:
            for y in range(fromy, toy+1):
                grid[y][fromx] = ROCK
        if fromy == toy:
            for x in range(fromx, tox+1):
                grid[fromy][x] = ROCK
display()

# sand produce physics
s = 0
while True:
    # Sand position
    posX = 500
    posY = 0

    # check fall
    void = False
    while True:
        # check if void
        if posY >= h-1:
            void = True
            break

        # apply physics
        if grid[posY+1][posX] == AIR:
            posY += 1
        elif grid[posY+1][posX-1] == AIR:
            posY += 1
            posX -= 1
        elif grid[posY+1][posX+1] == AIR:
            posY += 1
            posX += 1
        else:
            # no place to fall
            grid[posY][posX] = SAND
            break
    
    # if end
    if void:
        print("Spawned sand nr", s, "is the first one falling in void")
        break

    # Debug
    #print("Spawned Sand", s+1)
    # display()
    s += 1