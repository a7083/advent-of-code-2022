
# open file
f = open("input.txt", "r")

# for each line
total = 0
for l in f:
    line = l[:len(l)-1]
    left = line[:int(len(line)/2)]
    right = line[int(len(line)/2):int(len(line))]
    print(line)
    keepLooking = True
    for i in left:
        for ii in right:
            if not keepLooking:
                break
            if i == ii:
                ascii = ord(i)
                if (i.islower()):
                    total += ascii - 96
                    print("Found ", i, " adding ", ascii - 96)
                    keepLooking = False
                    break
                total += ascii - 38
                keepLooking = False
                print("Found ", i, " adding ", ascii - 38)
                break
    if keepLooking:
        print("Nothing found")
# End
print(total)
f.close()