
# open file
f = open("input.txt", "r")

# for each line
packs = ["", "", ""]

index = 0
total = 0

for l in f:
    line = l[:len(l)-1]
    print(line)

    packs[index%3] = line
    index += 1
    if index % 3 != 0:
        continue

    keepLooking = True
    for i in packs[0]:
        for ii in packs[1]:
            for iii in packs[2]:
                if not keepLooking:
                    break
                if i == ii == iii:
                    ascii = ord(i)
                    if (i.islower()):
                        total += ascii - 96
                        print("Found ", i, " adding ", ascii - 96)
                        keepLooking = False
                        break
                    total += ascii - 38
                    keepLooking = False
                    print("Found ", i, " adding ", ascii - 38)
                    break
    if keepLooking:
        print("Nothing found")
# End
print(total)
f.close()