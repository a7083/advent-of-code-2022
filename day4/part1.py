
# open file
f = open("input.txt", "r")

# for each line
total = 0
for l in f:
    # add borders to section
    index = 0
    section = ["", "", "", ""]
    for c in l:
        if c != '-' and c != ',' and c != '\n':
            section[index] += c
        else:
            index += 1
    
    # turn to int
    section[0] = int(section[0])
    section[1] = int(section[1])
    section[2] = int(section[2])
    section[3] = int(section[3])

    print(section)

    # check if its inside
    if section[2] >= section[0] and section[3] <= section[1] \
        or section[0] >= section[2] and section[1] <= section[3]:
        print("Its inside!")
        total += 1
# End
print(total)
f.close()