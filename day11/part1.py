class Monkey:
    items = []
    operation = ""
    divisible = 0
    trueThrow = 0
    falseThrow = 0
    
    inspectedItems = 0

# read file
f = open("input.txt", "r")
lines = f.readlines()

# var
monkeys = []

# set monkey objects
i = 0
while i < len(lines):
    # new monkey
    m = Monkey()
    m.items = list(map(int, lines[i+1].replace("\n", "").replace(",", "").split(" ")[4:]))
    m.operation = lines[i+2].replace("\n", "")[23:]
    m.divisible = int(lines[i+3].replace("\n", "").split(" ")[5])   
    m.trueThrow = int(lines[i+4].replace("\n", "").split(" ")[9])
    m.falseThrow = int(lines[i+5].replace("\n", "").split(" ")[9])
    monkeys.append(m)
    
    i += 7

    
# rounds
i = 0
for r in range(20):
    print("Round", r)
    for m in monkeys:
        print("Monkey", i)
        for item in m.items:
            print("Inspects worry level", item)
            print(m.items, m.operation, m.divisible, m.trueThrow, m.falseThrow)
            tmpOperation = m.operation.replace("old", str(item))
            w = int(eval(str(item) + tmpOperation))
            print("New worry level is", w)
            w = w // 3
            print("New devided worry level is", w)
            if w % m.divisible == 0:
                monkeys[m.trueThrow].items.append(w)
                print("Passed to", m.trueThrow)
            else:
                monkeys[m.falseThrow].items.append(w)
                print("Passed to", m.falseThrow)
            m.inspectedItems += 1
        m.items = []
        print("")
        i += 1
    
    # print monkeys
    print("After round", r)
    for m in monkeys:
        print(m.items)

# total
print("Inspected items")
total = []
for i in range(len(monkeys)):
    print("Monkey", i, monkeys[i].inspectedItems)
    total.append(monkeys[i].inspectedItems)
total.sort(reverse=True)
print("Result:", total[0]*total[1])
