class Monkey:
    items = []
    operation = ""
    divisible = 0
    trueThrow = 0
    falseThrow = 0
    
    inspectedItems = 0

# read file
f = open("input.txt", "r")
lines = f.readlines()

# var
monkeys = []
multDivisor = 1

# set monkey objects
i = 0
while i < len(lines):
    # new monkey
    m = Monkey()
    m.items = list(map(int, lines[i+1].replace("\n", "").replace(",", "").split(" ")[4:]))
    m.operation = lines[i+2].replace("\n", "")[23:]
    m.divisible = int(lines[i+3].replace("\n", "").split(" ")[5])   
    m.trueThrow = int(lines[i+4].replace("\n", "").split(" ")[9])
    m.falseThrow = int(lines[i+5].replace("\n", "").split(" ")[9])
    monkeys.append(m)
    
    multDivisor *= m.divisible
    i += 7

# rounds
i = 0
devidor = 3
for r in range(10000):
    for m in monkeys:
        for item in m.items:
            tmpOperation = m.operation.replace("old", str(item))
            w = int(eval(str(item) + tmpOperation))
            
            # modulo the multiple of all devisors 
            w = w % multDivisor
            
            if w % m.divisible == 0:
                monkeys[m.trueThrow].items.append(w)
            else:
                monkeys[m.falseThrow].items.append(w)
            m.inspectedItems += 1
        m.items = []
        i += 1
    
    if r % 1000 != 0: continue
    # print monkeys
    print("Inspected items after round", r)
    for i in range(len(monkeys)):
        print("Monkey", i, monkeys[i].inspectedItems)

# total
print("\n\nInspected items total")
total = []
for i in range(len(monkeys)):
    print("Monkey", i, monkeys[i].inspectedItems)
    total.append(monkeys[i].inspectedItems)
total.sort(reverse=True)
print("Result:", total[0]*total[1])
