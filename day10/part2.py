# read file
f = open("input.txt", "r")

cycle = 0
x = 1  

def incrCycle():
    global cycle
    global x
    global sums
    
    #print("During cycle:", cycle, "X:", x)
     # display
    if cycle%40 >= x-1 and cycle%40 <= x+1:
        print("#", end="")
    else:
        print(".", end="")
        
    if cycle != 0 and (cycle+1) % 40 == 0:
        print("")
        
    cycle += 1

# each instruction
for line in f:
    l = line.replace("\n", "")
    
    if l.split(" ")[0] == "addx":
        incrCycle()
        incrCycle()
        x += int(l.split(" ")[1])
        #print(int(l.split(" ")[1]))
    else:
        # noop
        incrCycle()
        #print("noop")

# print
print("End of instructions. Cycle:", cycle, "X:", x)
