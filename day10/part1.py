# read file
f = open("input.txt", "r")

cycle = 0
x = 1  

sums = 0

def incrCycle():
    global cycle
    global x
    global sums
    cycle += 1
    if (cycle == 20 or (cycle-20) % 40 == 0):
        print("During cycle:", cycle, "X:", x, "Signal:", cycle*x)
        sums += cycle*x

# each instruction
for line in f:
    l = line.replace("\n", "")
    
    if l.split(" ")[0] == "addx":
        incrCycle()
        incrCycle()
        x += int(l.split(" ")[1])
        #print(int(l.split(" ")[1]))
    else:
        # noop
        incrCycle()
        #print("noop")

# print
print("End of instructions. Cycle:", cycle, "X:", x)
print("Sums:", sums)