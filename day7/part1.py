class Node:
    previous = None
    next = []
    value = ""
    size = 0
    typ = ""

# open file
f = open("input.txt", "r")

# build graph
startNode = Node()
startNode.typ = "dir"
startNode.value = "/"
currentNode = startNode
for l in f:
    line = l[:len(l)-1]
    # print("Handling line", line, line.split(" "))
    # get command
    if line[0] == '$':
        if line.split(" ")[1] == "cd":
            # root
            if line.split(" ")[2] == "/":
                print("Going back to root dir")
                currentNode = startNode
                continue
            # previous
            if line.split(" ")[2] == "..":
                print("Going up one dir")
                currentNode = currentNode.previous
                continue
            # enter
            for n in currentNode.next:
                print("Looking for", line.split(" ")[2], "found", n.value)
                if n.value == line.split(" ")[2]:
                    print("Found correct one")
                    
                    currentNode = n
                    break
        if line.split(" ")[1] == "ls":
            pass
    else:
        # it's file or dir
        if line.split(" ")[0] == "dir":
            tmp = Node()
            tmp.next = []
            tmp.size = 0
            tmp.value = line.split(" ")[1]
            tmp.previous = currentNode
            tmp.typ = "dir"
            currentNode.next.append(tmp)
            print("Added new dir node", tmp.value, "to", currentNode.value)
        else:
            tmp = Node()
            tmp.next = []
            tmp.size = line.split(" ")[0]
            tmp.value = line.split(" ")[1]
            tmp.previous = currentNode
            # add size to upper ones
            prev = tmp.previous
            while prev != None:
                prev.size += int(tmp.size)
                prev = prev.previous
            
            tmp.typ = "file"
            currentNode.next.append(tmp)
            if tmp.value == "a":
                print(len(tmp.next))
            print("Added new file", tmp.value, tmp.size, "to", currentNode.value)

# print 
visited = set()
sizes = list()
def dfs(node, level):
    if node in visited:
        return
    tabs = "    " * level
    print(tabs, "-", node.value, "(", node.typ, ",", node.size, ")")

    # add all with size < 100.000
    if node.typ == "dir" and node.size < 100000:
        sizes.append(node.size)
        print("added to sum")

    visited.add(node)
    if not node.next:
        return
    for n in node.next:
        dfs(n, level+1)

dfs(startNode, 0)

sum = 0
for s in sizes:
    sum += s
print("Sum:", sum)

f.close()