
# open file
f = open("input.txt", "r")

w = 6
h = 5

sx = 0
sy = 0

total = 10
knots = []
for i in range(total):
    knots.append({'x': sx, 'y':sy})

visited = set()

def display():
    for y in reversed(range(h)):
        for x in range(w):
            printed = False
            for i in range(total):
                if printed: continue
                if knots[i]['x'] == x and knots[i]['y'] == y:
                    print(i, end="")
                    printed = True
            if not printed:
                print(".", end="")
        print("")
    print("")

# each instruction
for line in f:
    dir = line[0]
    steps = int(line[2:4])
    # print("----", line[:len(line)-1], "----")
    #print(dir, steps)

    # each step
    for s in range(steps):

        # apply dir to head
        if dir == "R":
            knots[0]['x'] += 1
        if dir == "L":
            knots[0]['x'] -= 1
        if dir == "U":
            knots[0]['y'] += 1
        if dir == "D":
            knots[0]['y'] -= 1

        # for each tail
        for i in range(1, total):
            # apply tail dif
            difX = knots[i-1]['x'] - knots[i]['x']
            difY = knots[i-1]['y'] - knots[i]['y']
            #print(hx, hy, tx, ty)
            #print("Dif:", difX, difY)
            
            # if it touches, stand still
            if (abs(difX) == 1 and abs(difY) == 1) \
                or (abs(difX) == 1 and abs(difY) == 0) \
                or (abs(difX) == 0 and abs(difY) == 1) \
                or (difX == 0 and difY == 0):
                    #print("Tail touches head, keeps position")
                    pass
            else:
                # if it's in same row/column
                if knots[i-1]['x'] == knots[i]['x'] or knots[i-1]['y'] == knots[i]['y']:
                    if difX > 1: difX = 1
                    if difX < -1: difX = -1
                    if difY > 1: difY = 1
                    if difY < -1: difY = -1
                    knots[i]['x'] += difX
                    knots[i]['y'] += difY
                    #print("Same r/c . Follow dif:", difX, difY)
                
                # jump diagonal
                elif difX != 0 and difY != 0:
                    if difX > 1: difX = 1
                    if difX < -1: difX = -1
                    if difY > 1: difY = 1
                    if difY < -1: difY = -1
                    knots[i]['x'] += difX
                    knots[i]['y'] += difY
                    #print("Diagonaljump . Follow dif:", difX, difY)

        # add to visited
        pair = (knots[9]['x'], knots[9]['y'])
        visited.add(pair)
        #display()

print("Tail positions visited:", len(visited))
#print(visited)

f.close()