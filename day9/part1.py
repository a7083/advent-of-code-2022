
# open file
f = open("input.txt", "r")

w = 6
h = 5

sx = 0
sy = 0

hx = sx
hy = sy
tx = sx
ty = sy

allowTailFollow = False

visited = set()

def display():
    for y in reversed(range(h)):
        for x in range(w):
            if hx == x and hy == y:
                print("H", end="")
            elif tx == x and ty == y:
                print("T", end="")
            else:
                print(".", end="")
        print("")
    print("")

# each instruction
for line in f:
    dir = line[0]
    steps = int(line[2:4])
    #print("----", line[:len(line)-1], "----")
    #print(dir, steps)

    # each step
    for s in range(steps):

        # apply dir to head
        if dir == "R":
            hx += 1
        if dir == "L":
            hx -= 1
        if dir == "U":
            hy += 1
        if dir == "D":
            hy -= 1

        # apply tail dir
        difX = hx - tx
        difY = hy - ty
        #print(hx, hy, tx, ty)
        #print("Dif:", difX, difY)
        
        # if it touches, stand still
        if (abs(difX) == 1 and abs(difY) == 1) \
            or (abs(difX) == 1 and abs(difY) == 0) \
            or (abs(difX) == 0 and abs(difY) == 1) \
            or (difX == 0 and difY == 0):
                #print("Tail touches head, keeps position")
                pass
        else:
            # if it's in same row/column
            if hx == tx or hy == ty:
                if difX > 1: difX = 1
                if difX < -1: difX = -1
                if difY > 1: difY = 1
                if difY < -1: difY = -1
                tx += difX
                ty += difY
                #print("Same r/c . Follow dif:", difX, difY)
            
            # jump diagonal
            elif difX != 0 and difY != 0:
                if difX > 1: difX = 1
                if difX < -1: difX = -1
                if difY > 1: difY = 1
                if difY < -1: difY = -1
                tx += difX
                ty += difY
                #print("Diagonaljump . Follow dif:", difX, difY)

        # add to visited
        tailPos = (tx, ty)
        visited.add(tailPos)
        
    
        # display()

print("Tail positions visited:", len(visited))
#print(visited)

f.close()